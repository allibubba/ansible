see /READEM.md
depreciated

# -C (capital at the end) does a dry run, no actual changes


## Siege : Managed
// Build 2 Siege servers, uses recipe in tasks/siege-rollout.yml
ansible-playbook -c local -M modules -i files/inventory.ini tasks/siege-rollout.yml -C

## Default : Managed
// Build 2 Ansible Default, uses recipe in tasks/default-managed-rollout.yml
ansible-playbook -c local -M modules -i files/inventory.ini tasks/default-managed-rollout.yml -C


## Default : RH
// Build 2 new Default Ansible servers, uses recipe in tasks/default-rollout.yml
ansible-playbook -c local -M modules -i files/inventory.ini tasks/default-rollout.yml -C


## Install settings on new Siege machines
// NOTE: remove local connection
ansible-playbook -M modules -i files/inventory.ini tasks/siege-install.yml -C


## Run siege based on settings in files/siege-siegesrc and files/siege-urls.txt
// NOTE: remove local connection
ansible-playbook -M modules -i files/inventory.ini tasks/siege-start.yml -C

## stop
ansible-playbook -M modules -i files/inventory.ini tasks/siege-stop.yml -C